# Experiment Inventory

This is a basic Dropwizard service 2.0.10 service https://www.dropwizard.io/en/latest/ 
that I used for experimenting with Elasticsearch. It includes: 
* MariaDB DAO with tests
* A basic schema and model for the hold of inventory.
* Uses the official Elasticsearch client to read and write to Elasticsearch 

Versions:
* MariaDB `mysql  Ver 15.1 Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64) using readline 5.2`
  * InnoDB version 10.3.22
* Elasticsearch 7.8.1
* Python 3.7.5

How to start the ExpInventory application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/exp-inventory-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`
1. Verify Metrics endpoint `http://localhost:8081/metrics?pretty=true`
1. To see your applications health enter url `http://localhost:8081/healthcheck`

### Configuring IntelliJ
![runserver](documentation/dropwziard_intellij_runserver.png)

![runtest](documentation/dropwziard_intellij_testsetup.png)

### Enable the query log in the DB:
Assuming you installed MySQL with via APT in Ubuntu https://stackoverflow.com/questions/6479107/how-to-enable-mysql-query-log

Added the following to /etc/mysql/my.cnf
```text
[mysqld]
general_log_file = /var/log/mysql/query.log
general_log      = 1
```
Then enable it in MySQL with the command 
```
sudo mysql -u root -p
SET global general_log = 1;
```
Then you can dump out all the SQL queries executing for your investigation.

Using the logs in `/var/log/mysql` you can see each time Dropwizard does a connection check `tail -F query.log | grep "SELECT 1"`

### Dropwizard API

bulkSearch 
```
http://localhost:8080/hold/bulkSearch
{
    "pondIds": [],
    "holdTypes": ["primary", "temp"],
    "status": [],
    "skus": ["SKU-100"]
}
```

### Elasticsearch Info
You will need to set up the index first
```json
{
   "version":9,
   "mapping_version":1,
   "settings_version":2,
   "aliases_version":1,
   "routing_num_shards":1024,
   "state":"open",
   "settings":{
      "index":{
         "search":{
            "slowlog":{
               "level":"info",
               "threshold":{
                  "fetch":{
                     "warn":"2s",
                     "trace":"10ms",
                     "debug":"10ms",
                     "info":"10ms"
                  },
                  "query":{
                     "warn":"1ms",
                     "trace":"1ms",
                     "debug":"1ms",
                     "info":"1ms"
                  }
               }
            }
         },
         "number_of_shards":"1",
         "provided_name":"hold",
         "creation_date":"1597529313140",
         "number_of_replicas":"1",
         "uuid":"HlFiakkIR3eKNxThEc2u0g",
         "version":{
            "created":"7080199"
         }
      }
   },
   "mappings":{
      "_doc":{
         "properties":{
            "qty":{
               "type":"integer"
            },
            "created_at":{
               "type":"date"
            },
            "holdType":{
               "type":"keyword"
            },
            "id":{
               "type":"integer"
            },
            "sku":{
               "type":"keyword"
            },
            "pondId":{
               "type":"integer"
            },
            "status":{
               "type":"keyword"
            }
         }
      }
   },
   "aliases":[
      
   ],
   "primary_terms":{
      "0":3
   },
   "in_sync_allocations":{
      "0":[
         "Y-AEw7ixTVaaFV131UaKwA"
      ]
   },
   "rollover_info":{
      
   }
}
```

### Database Setup

Setup
```sql
CREATE TABLE IF NOT EXISTS inventory (
            id BIGINT PRIMARY KEY AUTO_INCREMENT,
            location TINYTEXT,
            sku TINYTEXT,
            qty int,
            created_at timestamp default current_timestamp
            );
DROP TABLE inventory;
describe inventory;

DROP TABLE hold;
DROP TABLE pond;

SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table pond; SET FOREIGN_KEY_CHECKS = 1;
SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table hold; SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE IF NOT EXISTS pond (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    sku TINYTEXT,
    location TINYTEXT,
    qty int,
    created_at timestamp default current_timestamp
);

CREATE TABLE IF NOT EXISTS hold (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    pondId BIGINT,
    holdType TINYTEXT,
    status TINYTEXT,
    sku TINYTEXT,
    qty int,
    created_at timestamp default current_timestamp,
    CONSTRAINT `fk_pond_id` FOREIGN KEY (pondId) REFERENCES pond (id)
    ON DELETE SET NULL ON UPDATE SET NULL 
);

CREATE INDEX hold_sku_index ON hold (sku(255));
```

Example Queries
```sql
ELECT COUNT(*) FROM hold;
SELECT * FROM hold;
SELECT holdType, COUNT(*) FROM hold GROUP BY holdType;
SELECT * FROM pond;

SELECT * FROM hold h WHERE h.sku = 'SKU-01';

SELECT COUNT(*) FROM pond;
SELECT * FROM pond ORDER BY id DESC;

SELECT h.holdType, COUNT(*)
FROM pond p
LEFT OUTER JOIN hold h on p.id = h.pondId
GROUP BY h.holdType
;

SELECT * FROM hold WHERE 1
  AND holdType in ('primary','temp')
  AND (sku in ('SKU-100') OR pondId in (100,2000))
ORDER BY id LIMIT 100;
```

### Run Locust

* This locust file can create records for testing using the API `locust -f locustfile_createPrimary.py`
* This locust file will generate random bulk search queries `locust -f locustfile_bulkSearch.py`

![locust](documentation/elasticsearch_experiment_with_locust.png)

