from locust import HttpUser, task, between, constant
import json
import random

'''
In order to run this test locally (at least on Ubuntu) you will need the following.

pip3 install locust

'''

class BasicHoldUser(HttpUser):
    api_key = "API-KEY-default"
    wait_time = constant(1) #between(5, 9)

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.api_key = random.choice(["API-KEY-001", "API-KEY-222", "API-KEY-333"])
        pass

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        pass

    @task(1)
    def bulkSearch(self):
        if bool(random.getrandbits(1)):
            pondIds = random.sample(range(13424), 2)
            holdTypes = random.sample(["primary", "temp"], random.randint(0,2))
            skus = []
            #loc = "LOC-" + str(random.randrange(0, 10000))
        else:
            pondIds = []
            holdTypes = random.sample(["primary", "temp"], random.randint(0,2))
            skus = ["SKU-" + str(random.randrange(0, 10000))]
            #loc = "LOC-" + str(random.randrange(0, 10000))

        headers = {"content-type": "application/json", "apikey": self.api_key}

        payload = {
            "pondIds": pondIds,
            "holdTypes": holdTypes,
            "status": [],
            "skus": skus,
        }
        pond_response = self.client.post("/hold/bulkSearch",
                                    data=json.dumps(payload),
                                    headers=headers)
