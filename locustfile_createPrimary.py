from locust import HttpUser, task, between, constant
import json
import random

'''
In order to run this test locally (at least on Ubuntu) you will need the following.

pip3 install locust

'''

class BasicHoldUser(HttpUser):
    api_key = "API-KEY-default"
    wait_time = constant(1) #between(5, 9)

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.api_key = random.choice(["API-KEY-001", "API-KEY-222", "API-KEY-333"])
        pass

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        pass

    @task(1)
    def createHold(self):
        sku = "SKU-" + str(random.randrange(0, 100000))
        loc = "LOC-" + str(random.randrange(0, 10000))
        num_holds = random.randrange(0, 20)
        headers = {"content-type": "application/json", "apikey": self.api_key}

        payload = {
            #"id": 1,
            "sku": sku,
            "location": loc,
            "qty": 100
        }
        pond_response = self.client.post("/pond/",
                                    data=json.dumps(payload),
                                    headers=headers)
        pondId = pond_response.json()['id']

        payload = {
            #"id": 1,
            "pondId": pondId,
            "holdType": "primary",
            "status": "active",
            "sku": sku,
            "qty": 100
        }
        hold_response = self.client.post("/hold/",
                         data=json.dumps(payload),
                         headers=headers)
        holdId = hold_response.json()['id']

        for i in range(num_holds):
            self.client.post(f"/pond/{pondId}/hold/{holdId}",
                             data=json.dumps({}),
                             headers=headers)