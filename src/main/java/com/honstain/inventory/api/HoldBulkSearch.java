package com.honstain.inventory.api;

import java.util.List;

public class HoldBulkSearch {
    private List<Integer> pondIds;
    private List<String> holdTypes;
    private List<String> status;
    private List<String> skus;

    private Long from;
    private Integer pageSize;

    public HoldBulkSearch() {
        // Jackson deserialization
    }

    public List<Integer> getPondIds() {
        return pondIds;
    }

    public void setPondIds(List<Integer> pondIds) {
        this.pondIds = pondIds;
    }

    public List<String> getHoldTypes() {
        return holdTypes;
    }

    public void setHoldTypes(List<String> holdTypes) {
        this.holdTypes = holdTypes;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

    public List<String> getSkus() {
        return skus;
    }

    public void setSkus(List<String> skus) {
        this.skus = skus;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
