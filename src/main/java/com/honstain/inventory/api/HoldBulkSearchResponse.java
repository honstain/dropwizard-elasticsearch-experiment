package com.honstain.inventory.api;

import java.util.List;

public class HoldBulkSearchResponse {

    List<Hold> holds;
    // Where to start pagination from
    Long from;
    // How many results were requested
    Integer pageSize;
    // How many results we have here
    Long resultCount;

    public List<Hold> getHolds() {
        return holds;
    }

    public void setHolds(List<Hold> holds) {
        this.holds = holds;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getResultCount() {
        return resultCount;
    }

    public void setResultCount(Long resultCount) {
        this.resultCount = resultCount;
    }
}
