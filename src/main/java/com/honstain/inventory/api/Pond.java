package com.honstain.inventory.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Pond implements Comparable<Pond> {
    private Long id;
    private String sku;
    private String location;
    private Integer qty;

    public Pond() {
        // Jackson deserialization
    }

    public Pond(Long id, String sku, String location,  Integer qty) {
        this.id = id;
        this.sku = sku;
        this.location = location;
        this.qty = qty;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty
    public String getSku() {
        return sku;
    }

    public String getLocation() {
        return location;
    }

    @JsonProperty
    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pond that = (Pond) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sku, that.sku) &&
                Objects.equals(location, that.location) &&
                Objects.equals(qty, that.qty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sku, qty);
    }

    @Override
    public int compareTo(Pond inventory) {
        return id.compareTo(inventory.id);
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "id=" + id +
                ", sku='" + sku + '\'' +
                ", location='" + location + '\'' +
                ", qty=" + qty +
                '}';
    }
}
