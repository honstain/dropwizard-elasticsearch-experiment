package com.honstain.inventory.db;

import com.honstain.inventory.api.Hold;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface HoldTable {

    @SqlUpdate(
            "CREATE TABLE IF NOT EXISTS hold (\n" +
            "    id BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
            "    pondId BIGINT,\n" +
            "    holdType TINYTEXT,\n" +
            "    status TINYTEXT,\n" +
            "    sku TINYTEXT,\n" +
            "    qty int,\n" +
            "    created_at timestamp default current_timestamp,\n" +
            "    CONSTRAINT `fk_pond_id` FOREIGN KEY (pondId) REFERENCES pond (id) \n" +
            "    ON DELETE SET NULL ON UPDATE SET NULL \n" +
            ");"
    )
    void createCycleCountTable();

    @SqlUpdate("SET FOREIGN_KEY_CHECKS = 0;")
    void dropFKCheck();
    @SqlUpdate("TRUNCATE table hold;")
    void dropCycleCountTable();
    @SqlUpdate("SET FOREIGN_KEY_CHECKS = 1;")
    void setFKCheck();

    @SqlUpdate(
            "INSERT INTO hold (pondId, holdType, status, sku, qty) " +
            "VALUES (:pondId, :holdType, :status, :sku, :qty)"
    )
    @GetGeneratedKeys
    long insert(
            @Bind("pondId") Long pondId,
            @Bind("holdType") String holdType,
            @Bind("status") String status,
            @Bind("sku") String sku,
            @Bind("qty") Integer qty
    );

    @SqlUpdate(
            "UPDATE hold  " +
            "SET pondId = :pondId, status = :status, qty = :qty " +
            "WHERE id = :id"
    )
    void update(
            @Bind("id") Long id,
            @Bind("pondId") Long pondId,
            @Bind("status") String status,
            @Bind("qty") Integer qty
    );

    @SqlQuery("SELECT id, pondId, holdType, status, sku, qty FROM hold")
    @RegisterRowMapper(HoldMapper.class)
    List<Hold> selectAll();

    @SqlQuery("SELECT id, pondId, holdType, status, sku, qty FROM hold WHERE pondId = :pondId")
    @RegisterRowMapper(HoldMapper.class)
    List<Hold> selectByPondId(@Bind("pondId") Long pondId);

    @SqlQuery(
            "SELECT id, pondId, holdType, status, sku, qty " +
            "FROM hold " +
            "WHERE id = :id " +
            "FOR UPDATE")
    @RegisterRowMapper(HoldMapper.class)
    Hold lockAndSelectById(@Bind("id") Long id);
}
