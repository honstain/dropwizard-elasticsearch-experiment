package com.honstain.inventory.db;

import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.HoldBulkSearch;
import com.honstain.inventory.api.Pond;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PondAndHoldDao {

    Logger log = LoggerFactory.getLogger(PondAndHoldDao.class);

    private final Jdbi jdbi;
    private final PondTable pondTable;
    private final HoldTable holdTable;

    public PondAndHoldDao(Jdbi jdbi, PondTable pondTable, HoldTable holdTable) {
        this.jdbi = jdbi;
        this.pondTable = pondTable;
        this.holdTable = holdTable;
    }

    public Hold createHoldFromSource(Long sourcePondId, Long sourceHoldId) {
        return jdbi.inTransaction(h -> {
            Pond sourcePond = pondTable.lockAndSelectById(sourcePondId);
            Hold sourceHold = holdTable.lockAndSelectById(sourceHoldId);

            // ***********************************************************
            // Experimental code block, looking at the overhead of pulling all other holds in
            // ***********************************************************
            // TODO - decide how to calculate the total on this thing.
            // TODO - I went with the efficient way to start, but I may want to compare it with calculating it from all holds.
            //long start = System.currentTimeMillis();
            //List<Hold> holdList = holdTable.selectByPondId(sourcePondId);
            //int sellable = 0;
            //int held = 0;
            //for (Hold hold: holdList) {
            //    if (hold.getHoldType().equals("primary")) {
            //        sellable += hold.getQty();
            //    } else if (hold.getHoldType().equals("temp")) {
            //        held += hold.getQty();
            //    }
            //}
            //log.debug("getByPond sellable {} held {} ms {}", sellable, held, System.currentTimeMillis() - start);
            // ***********************************************************

            // Decrement the source hold qty
            Integer sourceHoldQty = sourceHold.getQty();
            if (sourceHoldQty < 1) {
                throw new RuntimeException(String.format("Insufficient qty on sourceHold id: %s", sourceHoldId));
            }
            holdTable.update(sourceHoldId, sourceHold.getPondId(), sourceHold.getStatus(), sourceHoldQty - 1);

            Long newHoldId = holdTable.insert(sourcePondId, "temp", "active", sourcePond.getSku(), 1);
            Hold result = holdTable.lockAndSelectById(newHoldId);
            return result;
        });
    }

    public List<Hold> bulkSearch(HoldBulkSearch holdBulkSearch) {
        List<Hold> result = this.jdbi.withHandle(handle -> {

            StringBuilder newQuery = new StringBuilder();
            // TODO - if there are no search values this would try and return everything. Might not be ideal, think more.
            newQuery.append("SELECT * FROM hold WHERE 1 ");
            if (!holdBulkSearch.getPondIds().isEmpty()) {
                newQuery.append("AND pondId in (<pondIds>) ");
            }
            if (!holdBulkSearch.getHoldTypes().isEmpty()) {
                newQuery.append("AND holdType in (<holdTypes>) ");
            }
            if (!holdBulkSearch.getStatus().isEmpty()) {
                newQuery.append("AND status in (<status>) ");
            }
            if (!holdBulkSearch.getSkus().isEmpty()) {
                newQuery.append("AND sku in (<skus>) " );
            }
            newQuery.append("ORDER BY id ");
            newQuery.append("LIMIT 100 ");

            Query query = handle.createQuery(newQuery.toString());
            if (!holdBulkSearch.getPondIds().isEmpty()) {
                query.bindList("pondIds", holdBulkSearch.getPondIds());
            }
            if (!holdBulkSearch.getHoldTypes().isEmpty()) {
                query.bindList("holdTypes", holdBulkSearch.getHoldTypes());
            }
            if (!holdBulkSearch.getStatus().isEmpty()) {
                query.bindList("status", holdBulkSearch.getStatus());
            }
            if (!holdBulkSearch.getSkus().isEmpty()) {
                query.bindList("skus", holdBulkSearch.getSkus());
            }

            return query.mapToBean(Hold.class)
                .list();
        });
        return result;
    }
}
