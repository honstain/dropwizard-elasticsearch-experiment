package com.honstain.inventory.elasticsearch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HoldDocument {
    private Long id;
    private Long pondId;
    private String holdType;
    private String status;
    private String sku;
    private int qty;
    //private Date createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPondId() {
        return pondId;
    }

    public void setPondId(Long pondId) {
        this.pondId = pondId;
    }

    public String getHoldType() {
        return holdType;
    }

    public void setHoldType(String holdType) {
        this.holdType = holdType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "HoldDocument{" +
                "id=" + id +
                ", pondId=" + pondId +
                ", holdType='" + holdType + '\'' +
                ", status='" + status + '\'' +
                ", sku='" + sku + '\'' +
                ", qty=" + qty +
                '}';
    }
}
