package com.honstain.inventory.resources;


import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.HoldBulkSearch;
import com.honstain.inventory.api.HoldBulkSearchResponse;
import com.honstain.inventory.api.Pond;
import com.honstain.inventory.db.HoldTable;
import com.honstain.inventory.db.PondAndHoldDao;
import com.honstain.inventory.db.PondTable;
import com.honstain.inventory.elasticsearch.HoldDocument;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.codahale.metrics.MetricRegistry.name;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryResource {

    Logger log = LoggerFactory.getLogger(InventoryResource.class);

    public final Jdbi jdbi;
    public final PondTable pondTable;
    public final HoldTable holdTable;
    public final PondAndHoldDao pondAndHoldDao;
    public final RestHighLevelClient elasticSearchClient;
    public final MetricRegistry metricRegistry;
    public final Timer bulkSearchDBCall;
    public final Timer bulkSearchESCall;

    public InventoryResource(Jdbi jdbi, RestHighLevelClient elasticSearchClient, MetricRegistry metricRegistry) {
        this.elasticSearchClient = elasticSearchClient;
        this.jdbi = jdbi;
        this.holdTable = jdbi.onDemand(HoldTable.class);
        this.pondTable = jdbi.onDemand(PondTable.class);
        this.pondAndHoldDao = new PondAndHoldDao(jdbi, pondTable, holdTable);
        this.metricRegistry = metricRegistry;
        this.bulkSearchDBCall = this.metricRegistry.timer(name(InventoryResource.class, "bulkSearchDBCall"));
        this.bulkSearchESCall = this.metricRegistry.timer(name(InventoryResource.class, "bulkSearchESCall"));
    }

    @GET
    @Timed
    @Path("/pond")
    public List<Pond> getAllPonds() {
        List<Pond> result = this.pondTable.selectAll();
        result.sort(Comparator.comparing(Pond::getLocation).thenComparing(Pond::getSku));
        return result;
    }

    @POST
    @Timed
    @Path("/pond")
    public Pond createPond(Pond pond) {
        Long newPondId = this.pondTable.insert(pond.getSku(), pond.getLocation(), pond.getQty());
        Pond newPond = this.pondTable.lockAndSelectById(newPondId);
        return newPond;
    }

    @GET
    @Timed
    @Path("/hold/{holdId}")
    public Hold getHold(@PathParam("holdId") Long holdId) {
        try {
            HoldDocument holdDocument = searchESForHold(holdId.toString());
            return new Hold(
                    holdDocument.getId(),
                    holdDocument.getPondId(),
                    holdDocument.getHoldType(),
                    holdDocument.getStatus(),
                    holdDocument.getSku(),
                    holdDocument.getQty()
            );
        } catch(Exception ex) {
            log.error("ElasticSearch miss");
        }
        Hold hold = this.holdTable.lockAndSelectById(holdId);
        return hold;
    }

    @POST
    @Timed
    @Path("/hold")
    public Hold createHold(Hold hold) {
        Long newHoldId = this.holdTable.insert(hold.getPondId(), hold.getHoldType(), hold.getStatus(), hold.getSku(), hold.getQty());
        Hold newHold = this.holdTable.lockAndSelectById(newHoldId);
        upsertES(newHold);
        return newHold;
    }



    @POST
    @Timed
    @Path("/hold/bulkSearch")
    public HoldBulkSearchResponse holdBulkSearch(HoldBulkSearch holdBulkSearch) throws IOException {

        HoldBulkSearchResponse response = new HoldBulkSearchResponse();

        List<Hold> dbResult;
        try(final Timer.Context context = this.bulkSearchDBCall.time()) {
            dbResult = this.pondAndHoldDao.bulkSearch(holdBulkSearch);
        }
        response.setResultCount((long) dbResult.size());

        List<HoldDocument> esResult;
        try(final Timer.Context context = this.bulkSearchESCall.time()) {
             esResult = bulkSearchES(holdBulkSearch);
        }

        if (dbResult.size() == esResult.size()) {
            for (int i = 0; i<dbResult.size(); i++) {
                if (!dbResult.get(i).getId().equals(esResult.get(i).getId())) {
                    log.error("bulkSearch results not equal - dbResult id:{} esResult id:{}",
                            dbResult.get(i).getId(),
                            esResult.get(i).getId());
                }
            }
        } else {
            log.error("bulkSearch results not equal - dbResult.size:{} esResult.size:{}", dbResult.size(), esResult.size());
        }

        response.setHolds(esResult.stream().map(x ->
            new Hold(x.getId(), x.getPondId(), x.getHoldType(), x.getStatus(), x.getSku(), x.getQty())
        ).collect(Collectors.toList()));

        return response;
    }

    @POST
    @Timed
    @Path("/pond/{pondId}/hold/{holdId}")
    public Hold createHoldFromSource(@PathParam("pondId") Long pondId, @PathParam("holdId") Long holdId) {
        Hold newHold = this.pondAndHoldDao.createHoldFromSource(pondId, holdId);
        upsertES(newHold);
        return newHold;
    }

    @GET
    @Timed
    @Path("/slow")
    public void slow() {
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch(InterruptedException ex) { }
    }

    private List<HoldDocument> bulkSearchES(HoldBulkSearch bulkSearch) throws IOException {
        List<HoldDocument> result = new ArrayList<>();

        SearchRequest searchRequest = new SearchRequest("hold");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder temp = QueryBuilders.boolQuery();
        if (!bulkSearch.getPondIds().isEmpty()) {
            temp.must(QueryBuilders.termsQuery("pondId", bulkSearch.getPondIds()));
        }
        if (!bulkSearch.getHoldTypes().isEmpty()) {
            temp.must(QueryBuilders.termsQuery("holdType", bulkSearch.getHoldTypes()));
        }
        if (!bulkSearch.getStatus().isEmpty()) {
            temp.must(QueryBuilders.termsQuery("status", bulkSearch.getStatus()));
        }
        if (!bulkSearch.getSkus().isEmpty()) {
            temp.must(QueryBuilders.termsQuery("sku", bulkSearch.getSkus()));
        }
        searchSourceBuilder.query(temp);
        searchSourceBuilder.size(100);

        //searchSourceBuilder.query(QueryBuilders.boolQuery()
        //        .must(QueryBuilders.termsQuery("pondId", bulkSearch.getPondIds()))
        //        .must(QueryBuilders.termsQuery("holdType", bulkSearch.getHoldTypes()))
        //        .must(QueryBuilders.termsQuery("status", bulkSearch.getStatus()))
        //        .must(QueryBuilders.termsQuery("sku", bulkSearch.getSkus()))
        //);
        searchSourceBuilder.sort("id");
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = this.elasticSearchClient.search(searchRequest, RequestOptions.DEFAULT);
        RestStatus status = searchResponse.status();
        TimeValue took = searchResponse.getTook();
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();

        ObjectMapper mapper = new ObjectMapper();

        for (SearchHit hit : searchHits) {
            // do something with the SearchHit
            Map<String, Object> fields = hit.getSourceAsMap();
            //log.debug("ES hit: {}", fields.get("sku"));
            HoldDocument holdDocument = mapper.readValue(hit.getSourceAsString(), HoldDocument.class);
            //log.error("OBJECT: {}", holdDocument.toString());
            result.add(holdDocument);
        }
        //log.debug("ES call {} {} size:{}", status.getStatus(), took.getMillis(), hits.getHits().length);
        return result;
    }

    private HoldDocument searchESForHold(String id) throws IOException {
        SearchRequest searchRequest = new SearchRequest("movies");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchPhraseQuery("id", id));
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = this.elasticSearchClient.search(searchRequest, RequestOptions.DEFAULT);
        RestStatus status = searchResponse.status();
        TimeValue took = searchResponse.getTook();
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();

        ObjectMapper mapper = new ObjectMapper();

        for (SearchHit hit : searchHits) {
            // do something with the SearchHit
            Map<String, Object> fields = hit.getSourceAsMap();
            log.debug("ES hit: {}", fields.get("sku"));
            HoldDocument holdDocument = mapper.readValue(hit.getSourceAsString(), HoldDocument.class);
            log.error("OBJECT: {}", holdDocument.toString());
            return holdDocument;
        }
        log.debug("ES call {} {} size:{}", status.getStatus(), took.getMillis(), hits.getHits().length);
        return null;
    }

    private void upsertES(Hold hold) {
        IndexRequest request = new IndexRequest("hold");
        request.id(hold.getId().toString());
        try {
            request.source(new ObjectMapper().writeValueAsString(hold), XContentType.JSON);
            IndexResponse indexResponse = this.elasticSearchClient.index(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("Unable to convert object to json: {}", hold.toString());
        }
    }

}
