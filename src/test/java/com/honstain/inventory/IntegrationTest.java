package com.honstain.inventory;

import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.Pond;
import com.honstain.inventory.db.HoldTable;
import com.honstain.inventory.db.PondTable;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(DropwizardExtensionsSupport.class)
class IntegrationTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<ExpInventoryConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(ExpInventoryApplication.class, CONFIG_PATH);

    private static PondTable pondTable;
    private static HoldTable holdTable;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "maridadb-test"
        );
        pondTable = jdbi.onDemand(PondTable.class);
        pondTable.createCycleCountTable();

        holdTable = jdbi.onDemand(HoldTable.class);
        holdTable.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        holdTable.dropFKCheck();
        holdTable.dropCycleCountTable();
        pondTable.dropCycleCountTable();
        holdTable.setFKCheck();
    }

    @Test
    void testGetAllPonds() {
        final Pond pond1 = new Pond(1L, "SKU-01", "LOC-02", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, "SKU-01", "LOC-01", 2);
        pond2.setId(
                pondTable.insert(
                        pond2.getSku(),
                        pond2.getLocation(),
                        pond2.getQty()
                ));

        final List<Pond> expected = List.of(pond2, pond1);
        final List<Pond> ponds = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/pond")
                .request()
                .get(new GenericType<>() {});
        assertEquals(expected, ponds);
    }

    @Test
    void testCreatePonds() {
        final Pond pond1 = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, "SKU-01", "LOC-02", 2);

        final Pond responsePond = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/pond")
                .request()
                .post(Entity.json(pond2), Pond.class);

        final List<Pond> expected = List.of(pond1, responsePond);
        assertEquals(expected, pondTable.selectAll());
    }

    @Test
    void testCreateHold() {
        final Pond pond1 = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Hold hold = new Hold(-1L, pond1.getId(), "primary", "active", "SKU-01", 1);

        final Hold responsePond = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/hold")
                .request()
                .post(Entity.json(hold), Hold.class);

        assertEquals(responsePond, holdTable.lockAndSelectById(responsePond.getId()));
    }

    @Test
    void testCreateHoldFromSource() {
        final Pond pond = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond.setId(
                pondTable.insert(
                        pond.getSku(),
                        pond.getLocation(),
                        pond.getQty()
                ));

        final Hold primaryHold = new Hold(1L, pond.getId(), "primary", "active", "SKU-01", 1);
        primaryHold.setId(
                holdTable.insert(
                        primaryHold.getPondId(),
                        primaryHold.getHoldType(),
                        primaryHold.getStatus(),
                        primaryHold.getSku(),
                        primaryHold.getQty()
                ));

        final Hold newHold = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() +
                        String.format("/pond/%d/hold/%d", pond.getId(), primaryHold.getId()))
                .request()
                .post(Entity.json(null), Hold.class);

        primaryHold.setQty(0);
        assertEquals(List.of(primaryHold, newHold), holdTable.selectAll());
    }
}
